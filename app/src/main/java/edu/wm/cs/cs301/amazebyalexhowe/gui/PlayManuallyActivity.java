package edu.wm.cs.cs301.amazebyalexhowe.gui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.ToggleButton;

import edu.wm.cs.cs301.amazebyalexhowe.R;

public class PlayManuallyActivity extends AppCompatActivity {
    /**
     * Runs when the manual state is turned on
     * Sets up toggle buttons
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playing_manually);
        //
        ToggleButton map = toggleMap();
        ToggleButton sol = toggleSolution();
        ToggleButton wall = toggleWalls();
    }
    /**
     * Method used to operate the up button
     * @param view
     */
    public void up(View view){
        //redirect to next activity
        //Intent intent = new Intent(this, WinningActivity.class);
        //EditText editText = findViewById(R.id.editTextTextPersonName);
        //String message = editText.getText().toString();
        //intent.putExtra(EXTRA_MESSAGE, message);
        Toast toast = Toast.makeText(getApplicationContext(), "Moved up", Toast.LENGTH_SHORT);
        toast.show();
        Log.v("PlayManuallyActivity", "Up");
        //startActivity(intent);
    }
    /**
     * Method used to operate the down button
     * @param view
     */
    public void down(View view){
        //redirect to next activity
        //Intent intent = new Intent(this, WinningActivity.class);
        //EditText editText = findViewById(R.id.editTextTextPersonName);
        //String message = editText.getText().toString();
        //intent.putExtra(EXTRA_MESSAGE, message);
        Toast toast = Toast.makeText(getApplicationContext(), "Moved down", Toast.LENGTH_SHORT);
        toast.show();
        Log.v("PlayManuallyActivity", "Down");
        //startActivity(intent);
    }
    /**
     * Method used to operate the left button
     * @param view
     */
    public void left(View view){
        //redirect to next activity
        //Intent intent = new Intent(this, WinningActivity.class);
        //EditText editText = findViewById(R.id.editTextTextPersonName);
        //String message = editText.getText().toString();
        //intent.putExtra(EXTRA_MESSAGE, message);
        Toast toast = Toast.makeText(getApplicationContext(), "Moved left", Toast.LENGTH_SHORT);
        toast.show();
        Log.v("PlayManuallyActivity", "Left");
        //startActivity(intent);
    }
    /**
     * Method used to operate the right button
     * @param view
     */
    public void right(View view){
        //redirect to next activity
        //Intent intent = new Intent(this, WinningActivity.class);
        //EditText editText = findViewById(R.id.editTextTextPersonName);
        //String message = editText.getText().toString();
        //intent.putExtra(EXTRA_MESSAGE, message);
        Toast toast = Toast.makeText(getApplicationContext(), "Moved right", Toast.LENGTH_SHORT);
        toast.show();
        Log.v("PlayManuallyActivity", "Right");
        //startActivity(intent);
    }
    /**
     * Method used to operate the jump button
     * @param view
     */
    public void jump(View view){
        //redirect to next activity
        //Intent intent = new Intent(this, WinningActivity.class);
        //EditText editText = findViewById(R.id.editTextTextPersonName);
        //String message = editText.getText().toString();
        //intent.putExtra(EXTRA_MESSAGE, message);
        Toast toast = Toast.makeText(getApplicationContext(), "Jumped", Toast.LENGTH_SHORT);
        toast.show();
        Log.v("PlayManuallyActivity", "Jump");
        //startActivity(intent);
    }
    /**
     * makes the map toggleable
     * @return the button
     */
    private ToggleButton toggleMap(){
        ToggleButton toggle = (ToggleButton) findViewById(R.id.toggleButton);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            /**
             * Checks if the button was pushed and if it is true or false, logs accordingly
             * @param buttonView is button
             * @param isChecked is if the button is true or false
             */
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //The toggle is enabled
                    Toast toast = Toast.makeText(getApplicationContext(), "Map shown", Toast.LENGTH_SHORT);
                    toast.show();
                    Log.v("PlayManuallyActivity", "Enabled Map");
                } else {
                    // The toggle is disabled
                    Toast toast = Toast.makeText(getApplicationContext(), "Map hidden", Toast.LENGTH_SHORT);
                    toast.show();
                    Log.v("PlayManuallyActivity", "Disabled Map");
                }
            }
        });
        return toggle;
    }
    /**
     * makes the solution toggleable
     * @return the button
     */
    private ToggleButton toggleSolution(){
        ToggleButton toggle = (ToggleButton) findViewById(R.id.toggleButton2);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            /**
             * Checks if the button was pushed and if it is true or false, logs accordingly
             * @param buttonView is button
             * @param isChecked is if the button is true or false
             */
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //The toggle is enabled
                    Toast toast = Toast.makeText(getApplicationContext(), "Solution shown", Toast.LENGTH_SHORT);
                    toast.show();
                    Log.v("PlayManuallyActivity", "Enabled Solution");
                } else {
                    // The toggle is disabled
                    Toast toast = Toast.makeText(getApplicationContext(), "Solution hidden", Toast.LENGTH_SHORT);
                    toast.show();
                    Log.v("PlayManuallyActivity", "Disabled Solution");
                }
            }
        });
        return toggle;
    }
    /**
     * makes the walls toggleable
     * @return the button
     */
    private ToggleButton toggleWalls(){
        ToggleButton toggle = (ToggleButton) findViewById(R.id.toggleButton3);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            /**
             * Checks if the button was pushed and if it is true or false, logs accordingly
             * @param buttonView is button
             * @param isChecked is if the button is true or false
             */
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //The toggle is enabled
                    Toast toast = Toast.makeText(getApplicationContext(), "Walls shown", Toast.LENGTH_SHORT);
                    toast.show();
                    Log.v("PlayManuallyActivity", "Enabled Walls");
                } else {
                    // The toggle is disabled
                    Toast toast = Toast.makeText(getApplicationContext(), "Walls hidden", Toast.LENGTH_SHORT);
                    toast.show();
                    Log.v("PlayManuallyActivity", "Disabled Walls");
                }
            }
        });
        return toggle;
    }
    /**
     * Method used to operate the win button
     * @param view
     */
    public void debugWin(View view){
        //redirect to next activity
        Intent intent = new Intent(this, WinningActivity.class);
        //EditText editText = findViewById(R.id.editTextTextPersonName);
        //String message = editText.getText().toString();
        //intent.putExtra(EXTRA_MESSAGE, message);
        Toast toast = Toast.makeText(getApplicationContext(), "W eaten", Toast.LENGTH_SHORT);
        toast.show();
        Log.v("PlayManuallyActivity", "Win Game");
        startActivity(intent);
    }
}