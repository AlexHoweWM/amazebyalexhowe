package edu.wm.cs.cs301.amazebyalexhowe.gui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.ToggleButton;

import edu.wm.cs.cs301.amazebyalexhowe.R;

public class AMazeActivity extends AppCompatActivity {

    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    public static final String EXTRA_MESSAGE = "edu.wm.cs.cs301.gui.MESSAGE";
    protected int storalg = -1;
    protected int alg = 0;
    protected int storrooms = -1;
    protected int rooms = 0;
    protected int storprog = -1;
    protected int prog = 0;
    protected int storseed = -1;
    protected int seed = 0;

    /**
     * Runs on creation of the main menu, creates calls for parts so they operate properly
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref= getPreferences(Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        setContentView(R.layout.activity_main);
        Spinner spinner = spinAlg();
        Switch switcher = switchRooms();
        SeekBar seekbar = seekDiff();
        storalg = sharedPref.getInt("Algorithm", -1);
        storprog = sharedPref.getInt("Progress", -1);
        storrooms = sharedPref.getInt("Rooms", -1);
        storseed = sharedPref.getInt("Seed", -1);

    }

    /**
     * Turns on the spinner for the Algorithm
     * @return the spinner in case we need it for the future
     */
    private Spinner spinAlg (){
        final boolean[] dontcheckfirst = {false};
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            /**
             * Runs when something gets selected in the spinner
             * @param parent is the spinner
             * @param selectedItemView
             * @param pos is where the item is
             * @param id
             */
            @Override
            public void onItemSelected(AdapterView<?> parent, View selectedItemView, int pos, long id) {
                String text = "";
                switch((String) parent.getItemAtPosition(pos)){
                    case "DFS Algorithm":
                        text = "DFS Algorithm Chosen";
                        alg = 0;
                        break;
                    case "Prim Algorithm":
                        text = "Prim Algorithm Chosen";
                        alg = 1;
                        break;
                    case "Eller Algorithm":
                        text = "Eller Algorithm Chosen";
                        alg = 2;
                        break;
                    default:
                        text = (String) parent.getItemAtPosition(pos);
                        alg = 3;
                        break;
                }
                //this dont check first part is added so we don't get an instant toast whenever the main menu opens,
                if(dontcheckfirst[0] == true){
                    Log.v("AMazeActivity", text);
                    Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
                    toast.show();
                }
                dontcheckfirst[0] = true;
            }

            /**
             * Runs in case nothing is selected. I have yet to test it but if you do please let me know how
             * @param parent
             */
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // your code here
                Log.v("GeneratingActivity", "Nothing Selected");
                Toast toast = Toast.makeText(getApplicationContext(), "Nothing Selected", Toast.LENGTH_SHORT);
                toast.show();
            }

        });
        return spinner;
    }

    /**
     * Operates the switch button for the rooms
     * @return the switch
     */
    private Switch switchRooms(){
        Switch switcher = (Switch) findViewById(R.id.switch1);
        switcher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            /**
             * Checks to see if the button was pushed or not
             * @param buttonView
             * @param isChecked if the button is checked or not
             */
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //The toggle is enabled
                    Toast toast = Toast.makeText(getApplicationContext(), "Rooms allowed", Toast.LENGTH_SHORT);
                    rooms = 1;
                    toast.show();
                    Log.v("AMazeActivity", "Enabled Rooms");
                } else {
                    // The toggle is disabled
                    Toast toast = Toast.makeText(getApplicationContext(), "Rooms disallowed", Toast.LENGTH_SHORT);
                    rooms = 0;
                    toast.show();
                    Log.v("AMazeActivity", "Disabled Rooms");
                }
            }
        });
        return switcher;
    }

    /**
     * Operates the seek bar to determine the difficulty
     * @return the seek bar
     */
    private SeekBar seekDiff() {
        SeekBar seekbar = (SeekBar) findViewById(R.id.seekBar);
        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            //string used for storing the text for toasts and log
            private String text = "";

            /**
             * Checks to see if the bar's progress changed.
             * @param seekBar is the bar
             * @param i
             * @param b
             */
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                text = "Difficulty set to " + Integer.toString(seekbar.getProgress());
                prog = seekbar.getProgress();
            }

            /**
             * Runs when the bar is touched
             * @param seekBar is the bar
             */
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                Log.v("AMazeActivity", "Touching seekBar");
            }

            /**
             * Runs when the bar is no longer being touched
             * @param seekBar is the bar
             */
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
                toast.show();
                Log.v("AMazeActivity", text);
            }
        });
        return seekbar;
    }

    /**
     * call when user clicks button
     *
     */
    public void generateMaze(View view){
        //redirect to next activity
        Intent intent = new Intent(this, GeneratingActivity.class);
        //EditText editText = findViewById(R.id.editTextTextPersonName);
        //String message = editText.getText().toString();
        seed=alg+rooms+prog; // this creates semi-unique seeds, purely for debug purposes; if actually implemented it could store an actual randomly generated seed
        String message = Integer.toString(alg)+","+Integer.toString(rooms)+","+Integer.toString(prog)+","+Integer.toString(seed);
        intent.putExtra(EXTRA_MESSAGE, message);
        Toast toast = Toast.makeText(getApplicationContext(), "Generating Maze", Toast.LENGTH_SHORT);
        toast.show();
        Log.v("AMazeActivity", "Generating Maze");
        editor.putInt("Algorithm", alg);
        editor.putInt("Rooms", rooms);
        editor.putInt("Progress", prog);
        editor.putInt("Seed", seed);
        editor.apply();
        startActivity(intent);
    }

    /**
     * Call when revisit is clicked
     * @param view
     */
    public void revisitMaze(View view){
        //redirect to next activity
        Log.v("AMazeActivity", "Revisiting Maze");
        storalg = sharedPref.getInt("Algorithm", -1);
        storprog = sharedPref.getInt("Progress", -1);
        storrooms = sharedPref.getInt("Rooms", -1);
        storseed = sharedPref.getInt("Seed", -1);
        if(storalg != -1){
            alg = storalg;
            Log.v("AMazeActivity", "Set alg to "+Integer.toString(alg));
        }
        if(storrooms != -1){
            rooms = storrooms;
            Log.v("AMazeActivity", "Set rooms to "+Integer.toString(rooms));
        }
        if(storprog != -1){
            prog = storprog;
            Log.v("AMazeActivity", "Set progress to "+Integer.toString(prog));
        }
        if(storseed != -1) {
            seed = storseed;
            Log.v("AMazeActivity", "Set seed to "+Integer.toString(seed));
        }

        generateMaze(view);
    }
}