package edu.wm.cs.cs301.amazebyalexhowe.gui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import edu.wm.cs.cs301.amazebyalexhowe.R;

import static android.view.View.MeasureSpec.getMode;

public class MazePanel extends View {
    Bitmap bitmap;
    Context context;
    public MazePanel(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    @Override
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.FILL);
        Rect cropRect = new Rect(0,0,1400,600);
        canvas.drawRect(cropRect, paint);
    }

    @Override
    protected void onMeasure(int widthmeasure, int heightmeasure) {
        //set how big we want the screen
        int iwidth = 2500;
        int iheight = 300;
        // get what mode the panel is in and the panel size
        int wmode = getMode(widthmeasure);
        int hmode = getMode(heightmeasure);
        int width;
        int height;

        //set Width
        if (wmode == MeasureSpec.EXACTLY) {
            width = MeasureSpec.getSize(widthmeasure);
        } else if (wmode == MeasureSpec.AT_MOST) {
            width = Math.min(iwidth, MeasureSpec.getSize(widthmeasure));
        } else {
            //Be whatever you want
            width = iwidth;
        }

        //set Height
        if (hmode == MeasureSpec.EXACTLY) {
            height = MeasureSpec.getSize(heightmeasure);
        } else if (hmode == MeasureSpec.AT_MOST) {
            height = Math.min(iheight, MeasureSpec.getSize(heightmeasure));
        } else {
            height = iheight;
        }
        setMeasuredDimension(width, height);
    }

    public void update(Canvas g) {
        //paint(g);
    }
    public void update() {
        //paint(getGraphics());
    }

    public void paint(Canvas g) {
        /*if (null == g) {
            System.out.println("MazePanel.paint: no graphics object, skipping drawImage operation");
        }
        else {
            g.drawImage(bufferImage,0,0,null);
        }*/
    }
    public Canvas getBufferGraphics() {
        // if necessary instantiate and store a graphics object for later use
        /*
        if (null == graphics) {
            if (null == bufferImage) {
                bufferImage = createImage(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
                if (null == bufferImage)
                {
                    System.out.println("Error: creation of buffered image failed, presumedly container not displayable");
                    return null; // still no buffer image, give up
                }
            }
            graphics = (Graphics2D) bufferImage.getGraphics();
            if (null == graphics) {
                System.out.println("Error: creation of graphics for buffered image failed, presumedly container not displayable");
            }
            else {
                // System.out.println("MazePanel: Using Rendering Hint");
                // For drawing in FirstPersonDrawer, setting rendering hint
                // became necessary when lines of polygons
                // that were not horizontal or vertical looked ragged
                graphics.setRenderingHint(java.awt.RenderingHints.KEY_ANTIALIASING,
                        java.awt.RenderingHints.VALUE_ANTIALIAS_ON);
                graphics.setRenderingHint(java.awt.RenderingHints.KEY_INTERPOLATION,
                        java.awt.RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            }
        }
        return graphics;*/
        return null;
    }

    public void setFont(String font) {
     //   markerFont = Font.decode(font);
    }

    public void commit() {
        // TODO Auto-generated method stub
        //paint(graphics);
    }
    public boolean isOperational() {
        // TODO Auto-generated method stub
        //if (null == graphics)
        //    return false;
        return true;
    }

    public void setColor(int rgb) {
        // TODO Auto-generated method stub
        /*
        colorint = rgb;
        color = initColor(colorint);
        graphics.setColor(color);*/
    }

    public void setColor(Color color) {
        // TODO Auto-generated method stub
        /*colorint = convertColor(color.getRed(), color.getGreen(), color.getBlue());
        this.color = color;
        graphics.setColor(this.color);
        //System.out.println("I am getting color " + Integer.toString(colorint));*/
    }

    public int getColor() {
        // TODO Auto-generated method stub
        //return colorint;
        return 0;
    }

    public static int convertColor(int r, int g, int b) {
        return 255*255*r+255*g+b;
    }

    public Color initColor(int color) {
        /*int to_convert = color;
        int b = to_convert%255;
        to_convert = to_convert/255;
        int g = to_convert%255;
        to_convert = to_convert/255;
        int r = to_convert%255;
        return new Color(r,g,b);*/
        return null;
    }
    public int getWallColor(int distance, int cc, int extensionX) {
        //return findWallColor(distance, cc, extensionX);
        return distance;
    }

    public static int findWallColor(int distance, int cc, int extensionX) {
        // TODO Auto-generated method stub
        /*
        final int d = distance / 4;
        // mod used to limit the number of colors to 6
        final int rgbValue = calculateRGBValue(d, extensionX);
        //System.out.println("Initcolor rgb: " + rgbValue);
        switch (((d >> 3) ^ cc) % 6) {
            case 0:
                return convertColor(rgbValue,20,20);
            case 1:
                return convertColor(20,60,20);
            case 2:
                return convertColor(20,20,rgbValue);
            case 3:
                return convertColor(rgbValue, 60, 20);
            case 4:
                return convertColor(20, 60, rgbValue);
            case 5:
                return convertColor(rgbValue,60,rgbValue);
            default:
                return convertColor(20,20,20);
        }*/
        return distance;
    }

    private static int calculateRGBValue(final int distance, int extensionX) {
        // compute rgb value, depends on distance and x direction
        // 7 in binary is 0...0111
        // use AND to get last 3 digits of distance
        /*final int part1 = distance & 7;
        final int add = (extensionX != 0) ? 1 : 0;
        final int rgbValue = ((part1 + 2 + add) * 70) / 8 + 80;
        return rgbValue;*/
        return distance;
    }

    public void addBackground(float percentToExit, int viewHeight, int viewWidth) {
        // TODO Auto-generated method stub
        // black rectangle in upper half of screen
        // graphics.setColor(Color.black);
        // dynamic color setting:
        /*
        graphics.setColor(getBackgroundColor(percentToExit, true));
        graphics.fillRect(0, 0, viewWidth, viewHeight/2);
        // grey rectangle in lower half of screen
        // graphics.setColor(Color.darkGray);
        // dynamic color setting:
        graphics.setColor(getBackgroundColor(percentToExit, false));
        graphics.fillRect(0, viewHeight/2, viewWidth, viewHeight/2);

         */
    }
    private Color getBackgroundColor(float percentToExit, boolean top) {
        /*return top? blend(initColor(convertColor(255,255,153)), initColor(convertColor(17,87,64)), percentToExit) :
                blend(Color.lightGray, initColor(convertColor(17,87,64)), percentToExit);*/
        return null;
    }

    private Color blend(Color c0, Color c1, double weight0) {
        /*
        if (weight0 < 0.1)
            return c1;
        if (weight0 > 0.95)
            return c0;
        double r = weight0 * c0.getRed() + (1-weight0) * c1.getRed();
        double g = weight0 * c0.getGreen() + (1-weight0) * c1.getGreen();
        double b = weight0 * c0.getBlue() + (1-weight0) * c1.getBlue();
        double a = Math.max(c0.getAlpha(), c1.getAlpha());

        return new Color((int) r, (int) g, (int) b, (int) a);*/
        return c0;
    }

    public void addFilledRectangle(int x, int y, int width, int height) {
        // TODO Auto-generated method stub
        //graphics.fillRect(x, y, width, height);
    }
    public void addFilledPolygon(int[] xPoints, int[] yPoints, int nPoints) {
        // TODO Auto-generated method stub
        //graphics.fillPolygon(xPoints, yPoints, nPoints);
    }
    public void addPolygon(int[] xPoints, int[] yPoints, int nPoints) {
        // TODO Auto-generated method stub
        //graphics.drawPolygon(xPoints, yPoints, nPoints);
    }
    public void addLine(int startX, int startY, int endX, int endY) {
        // TODO Auto-generated method stub
        //graphics.drawLine(startX, startY, endX, endY);
    }
    public void addFilledOval(int x, int y, int width, int height) {
        // TODO Auto-generated method stub
        //graphics.fillOval(x, y, width, height);
    }
    public void addArc(int x, int y, int width, int height, int startAngle, int arcAngle) {
        // TODO Auto-generated method stub
        //graphics.drawArc(x, y, width, height, startAngle, arcAngle);
    }
    public void addMarker(float x, float y, String str) {
        // TODO Auto-generated method stub
        /*
        setFont("Serif-PLAIN-16");
        if (null == markerFont) {
            System.out.println("herp derp");
        }
        GlyphVector gv = markerFont.createGlyphVector(graphics.getFontRenderContext(), str);
        Rectangle2D rect = gv.getVisualBounds();

        x -= rect.getWidth() / 2;
        y += rect.getHeight() / 2;

        graphics.drawGlyphVector(gv, x, y);*/
    }
}

