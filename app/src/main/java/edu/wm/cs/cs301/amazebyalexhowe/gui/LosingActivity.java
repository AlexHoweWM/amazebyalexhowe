package edu.wm.cs.cs301.amazebyalexhowe.gui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import edu.wm.cs.cs301.amazebyalexhowe.R;

public class LosingActivity extends AppCompatActivity {
    /**
     * Sets the content view to the xml file. Otherwise does nothing.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_losing);
    }
}