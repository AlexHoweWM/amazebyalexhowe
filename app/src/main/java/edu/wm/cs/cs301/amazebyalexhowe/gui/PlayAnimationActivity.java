package edu.wm.cs.cs301.amazebyalexhowe.gui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.ToggleButton;

import edu.wm.cs.cs301.amazebyalexhowe.R;

public class PlayAnimationActivity extends AppCompatActivity {
    /**
     * Runs on creation of animation state
     * adds various buttons and seekbar
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_animation);
        ToggleButton map = toggleMap();
        ToggleButton sol = toggleSolution();
        ToggleButton wall = toggleWalls();
        ToggleButton anim = toggleAnimation();
        SeekBar seekbar = seekAnimSpeed();
    }

    /**
     * makes the map toggleable
     * @return the button
     */
    private ToggleButton toggleMap(){
        ToggleButton toggle = (ToggleButton) findViewById(R.id.toggleButton4);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            /**
             * Checks if the button was pushed and if it is true or false, logs accordingly
             * @param buttonView is button
             * @param isChecked is if the button is true or false
             */
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //The toggle is enabled
                    Toast toast = Toast.makeText(getApplicationContext(), "Map shown", Toast.LENGTH_SHORT);
                    toast.show();
                    Log.v("PlayManuallyActivity", "Enabled Map");
                } else {
                    // The toggle is disabled
                    Toast toast = Toast.makeText(getApplicationContext(), "Map hidden", Toast.LENGTH_SHORT);
                    toast.show();
                    Log.v("PlayManuallyActivity", "Disabled Map");
                }
            }
        });
        return toggle;
    }
    /**
     * makes the solution toggleable
     * @return the button
     */
    private ToggleButton toggleSolution(){
        ToggleButton toggle = (ToggleButton) findViewById(R.id.toggleButton5);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            /**
             * Checks if the button was pushed and if it is true or false, logs accordingly
             * @param buttonView is button
             * @param isChecked is if the button is true or false
             */
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    //The toggle is enabled
                    Toast toast = Toast.makeText(getApplicationContext(), "Solution shown", Toast.LENGTH_SHORT);
                    toast.show();
                    Log.v("PlayManuallyActivity", "Enabled Solution");
                } else {
                    // The toggle is disabled
                    Toast toast = Toast.makeText(getApplicationContext(), "Solution hidden", Toast.LENGTH_SHORT);
                    toast.show();
                    Log.v("PlayManuallyActivity", "Disabled Solution");
                }
            }
        });
        return toggle;
    }
    /**
     * makes the walls toggleable
     * @return the button
     */
    private ToggleButton toggleWalls(){
        ToggleButton toggle = (ToggleButton) findViewById(R.id.toggleButton6);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            /**
             * Checks if the button was pushed and if it is true or false, logs accordingly
             * @param buttonView is button
             * @param isChecked is if the button is true or false
             */
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //The toggle is enabled
                    Toast toast = Toast.makeText(getApplicationContext(), "Walls shown", Toast.LENGTH_SHORT);
                    toast.show();
                    Log.v("PlayManuallyActivity", "Enabled Walls");
                } else {
                    // The toggle is disabled
                    Toast toast = Toast.makeText(getApplicationContext(), "Walls hidden", Toast.LENGTH_SHORT);
                    toast.show();
                    Log.v("PlayManuallyActivity", "Disabled Walls");
                }
            }
        });
        return toggle;
    }
    /**
     * makes the animation toggleable
     * @return the button
     */
    private ToggleButton toggleAnimation(){
        ToggleButton toggle = (ToggleButton) findViewById(R.id.toggleButton7);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            /**
             * Checks if the button was pushed and if it is true or false, logs accordingly
             * @param buttonView is button
             * @param isChecked is if the button is true or false
             */
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //The toggle is enabled
                    Toast toast = Toast.makeText(getApplicationContext(), "Animation started", Toast.LENGTH_SHORT);
                    toast.show();
                    Log.v("PlayManuallyActivity", "Start");
                } else {
                    // The toggle is disabled
                    Toast toast = Toast.makeText(getApplicationContext(), "Animation paused", Toast.LENGTH_SHORT);
                    toast.show();
                    Log.v("PlayManuallyActivity", "Pause");
                }
            }
        });
        return toggle;
    }

    /**
     * Operates the seekbar for animation speed
     * @return the seekbar
     */
    private SeekBar seekAnimSpeed() {
        SeekBar seekbar = (SeekBar) findViewById(R.id.seekBar2);
        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            /**
             * text stores message for toasts and log
             * onProgressChanged runs when the progress for the bar changes, it changes the text
             */
            private String text = "";
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                text = "Animation Speed set to " + Integer.toString(seekbar.getProgress());
            }

            /**
             * runs when bar is touched
             * @param seekBar is bar
             */
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                Log.v("PlayAnimationActivity", "Touching seekBar");
            }

            /**
             * runs when bar is no longer being touched, spits out text
             * @param seekBar is bar
             */
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
                toast.show();
                Log.v("PlayAnimationActivity", text);
            }
        });
        return seekbar;
    }

    /**
     * Method used to operate the win button
     * @param view
     */
    public void debugWin(View view){
        //redirect to next activity
        Intent intent = new Intent(this, WinningActivity.class);
        //EditText editText = findViewById(R.id.editTextTextPersonName);
        //String message = editText.getText().toString();
        //intent.putExtra(EXTRA_MESSAGE, message);
        Toast toast = Toast.makeText(getApplicationContext(), "W eaten", Toast.LENGTH_SHORT);
        toast.show();
        Log.v("PlayAnimationActivity", "Win Game");
        startActivity(intent);
    }

    /**
     * Method used to operate the lose button
     * @param view
     */
    public void debugLose(View view){
        //redirect to next activity
        Intent intent = new Intent(this, LosingActivity.class);
        //EditText editText = findViewById(R.id.editTextTextPersonName);
        //String message = editText.getText().toString();
        //intent.putExtra(EXTRA_MESSAGE, message);
        Toast toast = Toast.makeText(getApplicationContext(), "L eaten", Toast.LENGTH_SHORT);
        toast.show();
        Log.v("PlayAnimationActivity", "Lose");
        startActivity(intent);
    }
}