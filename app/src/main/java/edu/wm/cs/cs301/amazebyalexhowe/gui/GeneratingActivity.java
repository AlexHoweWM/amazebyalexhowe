package edu.wm.cs.cs301.amazebyalexhowe.gui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import edu.wm.cs.cs301.amazebyalexhowe.R;

public class GeneratingActivity extends AppCompatActivity {
    /*booleans used for corroborating both things needed to switch to a playing state
      thread is Complete is for the thread being true
      manual is checked is for if manual gets selected
      animation is checked is for if a robot is selected
     */
    private boolean threadIsComplete;
    private boolean manualIsChecked;
    private boolean animationIsChecked;

    /**
     * Runs on opening of the generating activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        threadIsComplete = false;
        manualIsChecked = false;
        animationIsChecked = false;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generating);
        //Spinner activities
        Spinner spinner = spinSensors();
        ProgressBar progressbar = (ProgressBar)findViewById(R.id.progressBar);
        progress(progressbar);
        // Get the Intent that started this activity and extract the string
        //Intent intent = getIntent();
        //String message = intent.getStringExtra(AMazeActivity.EXTRA_MESSAGE);

        // Capture the layout's TextView and set the string as its text
        //TextView textView = findViewById(R.id.textView);
        //textView.setText(message);
    }

    /**
     * Makes the spinner operational
     * @return the spinner
     */
    private Spinner spinSensors(){
        final boolean[] dontcheckfirst = {false};
        Spinner spinner = (Spinner) findViewById(R.id.spinner2);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            /**
             * Does things when an item is selected
             * @param parent is the spinner
             * @param selectedItemView
             * @param pos is the position of the item
             * @param id
             */
            @Override
            public void onItemSelected(AdapterView<?> parent, View selectedItemView, int pos, long id) {
                String text = "";
                switch((String) parent.getItemAtPosition(pos)){
                    case "Premium":
                        text = "Premium Chosen";
                        break;
                    case "Mediocre":
                        text = "Mediocre Chosen";
                        break;
                    case "So-So":
                        text = "So-So Chosen";
                        break;
                    case "Shaky":
                        text = "Shaky Chosen";
                        break;
                    default:
                        text = (String) parent.getItemAtPosition(pos);
                        break;
                }
                if(dontcheckfirst[0] == true){
                    Log.v("GeneratingActivity", text);
                    Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
                    toast.show();
                }
                dontcheckfirst[0] = true;
            }

            /**
             * Case when nothing gets selected
             * @param parent is the spinner
             */
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.v("GeneratingActivity", "Nothing Selected");
                Toast toast = Toast.makeText(getApplicationContext(), "Nothing Selected", Toast.LENGTH_SHORT);
                toast.show();
            }

        });
        return spinner;
    }

    /**
     * Makes the progress bar progress
     * @param progressbar
     */
    private void progress(ProgressBar progressbar){
        Thread thread = new Thread(new Runnable(){
            /**
             * Makes the thread run -- progresses bar, upon completion logs it and allows the menu to change if conditions are met
             */
           @Override
           public void run(){
               while (progressbar.getProgress() < 100){
                   try {
                       Thread.sleep(100);
                   } catch (InterruptedException e) {
                       e.printStackTrace();
                   }
                   progressbar.setProgress(progressbar.getProgress()+1);
               }
               Log.v("GeneratingActivity", "Thread completed, Progress Bar full");
               threadIsComplete = true;
               if (manualIsChecked == true){
                   Intent intent = new Intent(getApplicationContext(), PlayManuallyActivity.class);
                   startActivity(intent);
               }
               else if (animationIsChecked == true){
                   Intent intent = new Intent(getApplicationContext(), PlayAnimationActivity.class);
                   startActivity(intent);
               }
           }
        });
        thread.start();
    }

    /**
     * Operates radio buttons
     * @param view
     */
    public void playManually(View view){
        boolean checked = ((RadioButton) view).isChecked();
        Intent intent = null;
        String text = "default";
        // Check which radio button was clicked, act accordingly
        switch(view.getId()) {
            case R.id.radioButton:
            default:
                if (checked)
                    text = "starting in Manual Mode";
                    Log.v("GeneratingActivity", text);
                    intent = new Intent(this, PlayManuallyActivity.class);
                    manualIsChecked = true;
                    animationIsChecked = false;
                    break;
            case R.id.radioButton2:
            case R.id.radioButton3:
                if (checked)
                    text = "starting in Automatic Mode";
                    Log.v("GeneratingActivity", text);
                    intent = new Intent(this, PlayAnimationActivity.class);
                    animationIsChecked = true;
                    manualIsChecked = false;
                    break;
        }
        //Toast and open next menu if thread is done
        Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
        toast.show();
        if (threadIsComplete == true){
            startActivity(intent);
        }
    }
}